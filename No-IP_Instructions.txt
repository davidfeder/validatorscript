*STEP 1 - create a free or $2 account for No-IP http://noip.validatorstore.com/ and make a new domain name, you should also download and install their auto-updater software to keep it synced https://www.noip.com/download?page=linux

*STEP 2 - paste your new No-IP domain name at end of line 9 

*STEP 3 - paste line 9 at the end of the beacon conatiner line in all my other scripts

*Example --p2p-host-dns=validatorstore.noip.com

--p2p-host-dns=

*Example of full beacon conatiner line below, your's should have your new No-IP domain at the end of it below

sudo docker run -d --name beacon --network=host -v /home/admxn/blockchain:/blockchain registry.gitlab.com/pulsechaincom/prysm-pulse/beacon-chain:latest --pulsechain --jwt-secret=/blockchain/jwt.hex --datadir=/blockchain --checkpoint-sync-url=https://checkpoint.pulsechain.com --genesis-beacon-api-url=https://checkpoint.pulsechain.com --subscribe-all-subnets --p2p-max-peers 150 --min-sync-peers=1 --p2p-tcp-port=13000 --p2p-udp-port=12000 --suggested-fee-recipient=0x995f30748cD6F9bD2120864487BF799079e7BfBe --p2p-host-dns=validatorstore.noip.com