*INSTRUCTIONS: Download and save a copy of this script to your desktop folder - You MUST change on lines 31, 43, and 53 the ETH/PLS address to earn rewards (otherwise it burns)! On Line 43 you can change the --num_validators= wallets if you want more than 1. Optionally you can change line 53 graffiti to yours or leave it as validatorstore.com to show your support. If your Ubuntu/Linux username is not admxn then you must replace all instances of admxn with your username for all folder paths in the script, search for admxn using CTRL+F and replace all instances of admxn with your username*

sudo apt-get update -y && sudo apt-get upgrade -y && sudo apt-get dist-upgrade -y && sudo apt autoremove -y && sudo apt-get install apt-transport-https ca-certificates curl gnupg git ufw openssl lsb-release -y && sudo systemctl stop cups-browsed && sudo systemctl disable cups-browsed

sudo ufw allow 30303 && sudo ufw allow 12000 && sudo ufw allow 13000 && sudo ufw deny 631 && sudo ufw enable

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg

echo "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

sudo apt-get update -y

sudo apt-get install -y docker-ce docker-ce-cli containerd.io docker-compose

mkdir blockchain && sudo chmod 777 blockchain && cd blockchain

mkdir pw && sudo chmod 777 pw && cd pw

nano pw.txt

*INSTRUCTIONS: enter your prysm wallet password, hit CRTL+X, hit y, hit enter*

cd ~

openssl rand -hex 32 | sudo tee blockchain/jwt.hex > /dev/null

sudo docker run -d --name geth --network=host -v /home/admxn/blockchain:/blockchain registry.gitlab.com/pulsechaincom/go-pulse:latest --pulsechain --authrpc.jwtsecret=/blockchain/jwt.hex --datadir=/blockchain --state.scheme=path --maxpeers 150 --discovery.port 30303 --port 30303

*INSTRUCTIONS: line 31 Replace ETH/PLS address with YOUR address to earn rewards (otherwise it burns)*

sudo docker run -d --name beacon --network=host -v /home/admxn/blockchain:/blockchain registry.gitlab.com/pulsechaincom/prysm-pulse/beacon-chain:latest --pulsechain --jwt-secret=/blockchain/jwt.hex --datadir=/blockchain --checkpoint-sync-url=https://checkpoint.pulsechain.com --genesis-beacon-api-url=https://checkpoint.pulsechain.com --subscribe-all-subnets --p2p-max-peers 150 --min-sync-peers=1 --p2p-tcp-port=13000 --p2p-udp-port=12000 --suggested-fee-recipient=0x25f8Aaa00744cc0f67e64E81F876a699340962dD

git clone https://gitlab.com/pulsechaincom/staking-deposit-cli.git

cd staking-deposit-cli

sudo apt install python3-pip -y && sudo pip3 install -r requirements.txt && sudo python3 setup.py install

sudo ./deposit.sh install

*INSTRUCTIONS: for line 43: Replace ETH/PLS address with YOUR address to earn rewards (otherwise it burns), change the --num_validators= wallets if you want more than 1 *

sudo ./deposit.sh existing-mnemonic --num_validators=100 --chain=pulsechain --folder=/home/admxn/blockchain/ --eth1_withdrawal_address=0x25f8Aaa00744cc0f67e64E81F876a699340962dD

cd ~

sudo docker run -it --name validator -v /home/admxn/blockchain/validator_keys:/keys -v /home/admxn/blockchain/pw:/wallet registry.gitlab.com/pulsechaincom/prysm-pulse/validator:latest accounts import --keys-dir=/keys --wallet-dir=/wallet --pulsechain

sudo docker stop -t 180 validator && sudo docker container prune -f

*INSTRUCTIONS: line 53 Replace ETH/PLS address with YOUR address to earn rewards (otherwise it burns)*

sudo docker run -d --name validator --network=host -v /home/admxn/blockchain/validator_keys:/keys -v /home/admxn/blockchain/pw:/wallet registry.gitlab.com/pulsechaincom/prysm-pulse/validator:latest --pulsechain --wallet-dir=/wallet --wallet-password-file=/wallet/pw.txt --graffiti validatorstore.com --suggested-fee-recipient=0x25f8Aaa00744cc0f67e64E81F876a699340962dD

sudo docker update --restart always geth && sudo docker update --restart always beacon && sudo docker update --restart always validator

*INSTRUCTIONS: Enter the following three commands in seperate terminal windows each so you can watch what is happening for all three containers (geth, beacon, validator)*

sudo docker logs --follow geth

sudo docker logs --follow beacon

sudo docker logs --follow validator
